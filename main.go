package main

import (
	"crypto/rand"
	"fmt"
	"log"
	"math/big"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func main() {
	cnt := 5
	sep := " "
	fp, err := os.UserHomeDir()
	if err != nil {
		log.Fatalln(err)
	}
	fp = filepath.Join(fp, "Documents", "auth", "eff-large-wordlist.txt")
	args := os.Args
	if len(args) > 1 {
		var err error
		cnt, err = strconv.Atoi(args[1])
		if err != nil {
			log.Fatalln(err)
		}
		if len(args) > 2 {
			sep = args[2]
		}
		if len(args) > 3 {
			fp = args[3]
		}
	}
	b, err := os.ReadFile(fp)
	if err != nil {
		log.Fatalln(err)
	}
	wl := strings.Split(string(b), "\n")
	m := make(map[string]string)
	for _, v := range wl {
		c := strings.Split(strings.TrimSpace(v), "\t")
		if len(c) == 2 {
			m[c[0]] = c[1]
		}
	}
	i, k := 6, 1
	for i < len(m) {
		i *= 6
		k += 1
	}
	if i != len(m) {
		log.Fatalln("number of diceware words not a power of 6 / unsupported format")
	}
	p := make([]string, cnt)
	for cnt > 0 {
		key := ""
		for i := 0; i < k; i++ {
			bn, err := rand.Int(rand.Reader, big.NewInt(6))
			if err != nil {
				log.Fatalln(err)
			}
			key += strconv.Itoa(int(bn.Int64() + 1))
		}
		_, ok := m[key]
		if !ok {
			log.Fatalln("key not found")
		}
		p[cnt-1] = m[key]
		cnt--
	}
	fmt.Println(strings.Join(p, sep))
}
